const path = require("path");
const klaw = require("klaw");
const fs = require("fs-extra");
const hasha = require("hasha");
const globby = require("globby");
const cssnano = require("cssnano");
const minify = require("html-minifier").minify;
const { isMatch } = require("matcher");
const replace = require("replace-in-file");
const { injectManifest } = require("workbox-build");
const semverParse = require("semver/functions/parse");

function loadComponent(manifestsDirs, componentName) {
    for (const manifestsDir of manifestsDirs) {
        const manifestPath = path.join(manifestsDir, `${componentName}.json`);
        if (fs.existsSync(manifestPath)) {
            const manifest = fs.readJsonSync(manifestPath);
            return {
                manifest,
                directory: path.resolve(path.resolve(manifestPath), manifest.basePath)
            };
        }
    }
    throw new Error(`unknown component "${componentName}"`);
}

async function processResource(filepath, options, relDir) {
    const inputSuffix = options.suffix || path.extname(filepath);
    if (!filepath.endsWith(options.suffix))
        console.warn(`${filepath} does not end by suffix ${options.suffix}`);

    // TODO: using readFile sometimes returns an empty string; try to switch back
    let content = fs.readFileSync(filepath);

    /* process content if requested */
    if (options.processContent)
        content = await options.processContent(content, options.processContentOptions);

    /* compute hash, build new filename and create file in outputdir */
    const suffix = options.outputSuffix || inputSuffix;
    const revision = hasha(content, { algorithm: options.algorithm });
    const oldbasename = path.basename(filepath, inputSuffix);
    const newbasename = `${oldbasename}${suffix}`;
    const oldrelpath = relDir ? path.join(relDir, oldbasename) : oldbasename;
    let newrelpath = relDir ? path.join(relDir, newbasename) : newbasename;
    let assetName = oldrelpath.replace(/\//gu, ".");

    /* process asset output path if requested */
    if (options.processNames)
        [assetName, newrelpath] = options.processNames(assetName, filepath, newrelpath);

    const url = options.destDir ? path.join(options.destDir, newrelpath) : newrelpath;
    const outputPath = path.join(options.outputDir, url);
    await fs.ensureDir(path.dirname(outputPath));
    await fs.writeFile(outputPath, content);

    const info = {
        url,
        revision,
        size: content.length,
        type: options.type,
        preload: options.preload === undefined || options.preload
    };

    return [filepath, assetName, info];
}

class ResourceManager {

    constructor(componentName, { algorithm }) {
        this.componentName = componentName;
        this.algorithm = algorithm;
        this.extra = {};
        this.private = [];
        this.selects = [];
        this.available = {};
        this.components = {};
    }

    async createEntry(basePath, filePath, revision, baseUrl) {
        const fullPath = path.join(basePath, filePath);
        if (revision !== await hasha.fromFile(fullPath, { algorithm: this.algorithm }))
            process.stdout.write("computed and recorded revision of ${filePath} differ");

        return { url: `${baseUrl}/${filePath}`, revision };
    }

    async findAllResources() {
        for (const [componentName, { directory, manifest }] of Object.entries(this.components)) {
            /* resources */
            this.available[componentName] = {};
            for (const [name, resource] of Object.entries(manifest.resources)) {
                if (resource.preload) {
                    this.available[componentName][name] = await this.createEntry(
                        directory, resource.url, resource.revision, manifest.baseUrl
                    );
                }
            }

            /* private */
            if (manifest.private) {
                for (const info of manifest.private) {
                    this.private.push(
                        await this.createEntry(
                            directory, info.url, info.revision, manifest.baseUrl
                        )
                    );
                }
            }

            /* select */
            if (manifest.select)
                this.selects.push(...manifest.select);
        }
        // console.dir(this.available);
        // console.dir(this.selects);
    }

    async findModules(lang) {
        for (const { directory, manifest } of Object.values(this.components)) {
            const entry = await this.createEntry(
                directory,
                manifest.module.url,
                manifest.module.revision,
                manifest.baseUrl
            );
            if (lang in this.extra)
                this.extra[lang].push(entry);
            else
                this.extra[lang] = [entry];
        }
        // console.dir(this.extra);
    }

    async load(componentJson, dependencies, staticDir, manifestsDirs, outputDir) {

        this.components = {
            [this.componentName]: { directory: outputDir, manifest: componentJson }
        };

        /* recursively parse manifests of all dependencies */
        const queue = Object.keys(dependencies);

        while (queue.length) {
            const componentName = queue.pop();
            const component = loadComponent(manifestsDirs, componentName);
            this.components[componentName] = component;
            for (const dependency of component.manifest.dependencies) {
                if (queue.indexOf(dependency) === -1 && !(dependency in this.components))
                    queue.push(dependency);
            }
        }
    }

    async writeSelectedResources(lang, resourcesJsonPath) {
        const variant = `lang=${lang}`;
        const selected = {};
        for (const component of Object.keys(this.available)) {
            for (const [name, resource] of Object.entries(this.available[component])) {
                const m = /^([^[\]]*)(\[(.*)\])?$/u.exec(name);
                const assetName = `${component}:${m[1]}`;
                for (const select of this.selects) {
                    if (isMatch(assetName, select) && (!m[3] || m[3] === variant)) {
                        if (lang in this.extra)
                            this.extra[lang].push(resource);
                        else
                            this.extra[lang] = [resource];
                        if (!(component in selected))
                            selected[component] = {};
                        selected[component][m[1]] = resource.url;
                    }
                }
            }
        }
        console.log(lang);
        console.dir(selected);
        await fs.ensureDirSync(path.dirname(resourcesJsonPath));
        await fs.writeJson(resourcesJsonPath, selected);
        return selected;
    }
}

function formatComponentName(componentName, version) {
    const major = semverParse(version).major;
    return `${componentName}/v${major}`;
}

const RELATIVE_PREFIX = "relative:";
const EXTERNAL_PREFIX = "external:";
const COMPONENT_FILE = "component.json";

function plugin(options = {}) {

    const componentJsonPath = options.componentJsonPath || COMPONENT_FILE;
    const componentJson = fs.readJsonSync(componentJsonPath);
    const dependencies = componentJson.dependencies || {};

    // const packageJsonPath = options.packageJsonPath || "package.json";
    // const packageJson = fs.readJsonSync(packageJsonPath);

    if (!("name" in componentJson) || !("version" in componentJson))
        throw Error("package.json is missing name or version");

    const componentVersion = options.version || componentJson.version;
    const componentName = options.name || formatComponentName(componentJson.name, componentVersion);
    const componentLicense = options.license || componentJson.license;

    const algorithm = options.algorithm || "md5";
    const installDir = options.installDir || "dist";
    const staticUrl = options.staticUrl || "/static";
    const resourcesDir = options.resourcesDir || "../resources";
    const externalPrefix = options.externalPrefix || EXTERNAL_PREFIX;
    const staticDir = options.staticDir || path.join(installDir, "static");
    const outputDir = path.join(staticDir, componentName);
    fs.ensureDirSync(outputDir);

    const manifestsDir = options.manifestsDir || path.join(installDir, "manifests");
    const manifestsPath = options.manifestsPath || manifestsDir;
    const manifestsDirs = manifestsPath.split(":");
    fs.ensureDirSync(manifestsDir);

    const manifestPath = path.join(manifestsDir, `${componentName}.json`);
    const manifest = {
        name: componentName,
        version: componentVersion,
        license: componentLicense,
        dependencies,
        basePath: path.relative(manifestPath, outputDir),
        baseUrl: options.baseUrl || path.join(staticUrl, componentName),
        select: options.select ? options.select.map(
            s => (s.indexOf(":") === -1 ? `${componentName}:${s}` : s)
        ) : [],
        resources: {}
    };

    const manager = new ResourceManager(componentName, { algorithm });

    return {
        name: "component",
        resolveId(source, id) {
            if (!source.startsWith(externalPrefix))
                return null;
            const spec = source.slice(externalPrefix.length);
            const otherComponent = spec.split(":")[0];
            if (otherComponent !== componentName && !(otherComponent in dependencies))
                throw new Error(`component ${otherComponent} used but not declared`);
            return { id: spec, external: true };
        },
        async buildStart() {
            /* process resources, if any */
            if (options.resources) {
                for (const info of options.resources) {
                    if (info.resource === "single") {

                        const {
                            asset, suffix, outputSuffix, type, src, destDir, relDir,
                            processNames, processContent, processContentOptions, preload
                        } = info;

                        if (destDir)
                            await fs.ensureDir(path.join(outputDir, destDir));

                        const [filepath, assetName, data] = await processResource(src, {
                            suffix,
                            outputSuffix,
                            type,
                            relDir,
                            destDir,
                            preload,
                            algorithm,
                            outputDir,
                            processNames,
                            processContent,
                            processContentOptions
                        });

                        manifest.resources[asset/** @todo assetName?*/] = data;
                    }
                    else if (info.resource === "scan") {

                        const {
                            prefix, suffix, outputSuffix, type, srcDir, destDir,
                            processNames, processContent, processContentOptions,
                            recursive, ondone, select, preload
                        } = info;

                        await fs.ensureDir(path.join(outputDir, destDir));

                        const processingOptions = {
                            suffix,
                            outputSuffix,
                            type,
                            destDir,
                            preload,
                            algorithm,
                            outputDir,
                            processNames,
                            processContent,
                            processContentOptions
                        };

                        const promises = [];
                        const realSrcDir = await fs.realpath(srcDir);
                        const realSrcDirLength = realSrcDir.length + (srcDir.endsWith("/") ? 0 : 1);

                        await new Promise((resolve, reject) => klaw(srcDir, {
                            depthLimit: recursive ? -1 : 0
                        }).on("data", item => {
                            if (item.path.endsWith(suffix)) {
                                const filepath = item.path.slice(realSrcDirLength);
                                if (select && !select(filepath))
                                    return;
                                const relDir = recursive ? path.dirname(filepath) : null;
                                promises.push(
                                    processResource(item.path, processingOptions, relDir)
                                );
                            }
                        }).on("end", resolve));

                        const entries = await Promise.all(promises);

                        const translation = {};

                        /* build and write styles map */
                        for (const entry of entries) {
                            if (entry) {
                                const [filepath, assetName, info] = entry;
                                translation[filepath.slice(srcDir.length + 1)] = assetName;
                                // console.log("assetName", assetName);
                                manifest.resources[prefix ? `${prefix}.${assetName}` : assetName] = info;
                            }
                        }

                        if (ondone)
                            ondone(translation);
                    }
                    else if (info.resource === "glob") {

                        const {
                            prefix, suffix, type, patterns, destDir, processNames,
                            processContent, processContentOptions, ondone, select,
                            preload
                        } = info;

                        await fs.ensureDir(path.join(outputDir, destDir));

                        const processingOptions = {
                            suffix,
                            type,
                            destDir,
                            preload,
                            algorithm,
                            outputDir,
                            processNames,
                            processContent,
                            processContentOptions
                        };

                        const filepaths = await globby(patterns);

                        const promises = [];
                        for (const filepath of filepaths) {
                            if (select && !select(filepath))
                                return;
                            const relDir = path.dirname(filepath);
                            promises.push(
                                processResource(filepath, processingOptions, relDir)
                            );
                        }

                        const entries = await Promise.all(promises);
                        const translation = {};

                        /* build and write styles map */
                        for (const entry of entries) {
                            if (entry) {
                                const [filepath, assetName, info] = entry;
                                translation[filepath] = assetName;
                                // console.log("assetName", assetName);
                                manifest.resources[prefix ? `${prefix}.${assetName}` : assetName] = info;
                            }
                        }

                        if (ondone)
                            ondone(translation);
                    }
                    else
                        throw new Error(`unknown resource type ${info.resource}`);
                }
            }

            if (options.private) {
                manifest.private = [];
                for (const info of options.private) {
                    const dest = info.dest || info.src;
                    const outputFile = path.join(outputDir, dest);
                    let content = await fs.readFile(info.src);

                    if (info.process)
                        content = await Promise.resolve(info.process(content.toString()));
                    await fs.writeFile(outputFile, content);
                    const revision = hasha(content, { algorithm });
                    manifest.private.push({
                        url: dest, revision, size: content.length, type: info.type
                    });
                }
            }

            if (options.sw) {
                /** @todo compile sw.js ourselves? */
                const swManifest = await fs.readJson(options.sw.manifest);
                /** @todo we must know the format of path to file */
                manifest.sw = swManifest[options.sw.key].split("/")[1];
            }

            /* write a version of the manifest, which can be used by the main module */
            await fs.ensureDir(path.dirname(manifestPath));
            await fs.writeJson(manifestPath, manifest);

            if (options.workbox) {
                await manager.load(
                    manifest, dependencies, staticDir, manifestsDirs, outputDir
                );
                await manager.findAllResources();
                for (const lang of Object.keys(options.workbox)) {
                    await fs.ensureDir(path.join(resourcesDir, lang));
                    await manager.writeSelectedResources(
                        lang, path.join(resourcesDir, lang, "resources.json")
                    );
                }
            }
        },
        async writeBundle(bundle) {

            /* process main module */
            if (options.replace) {
                for (const [from, to] of options.replace)
                    await replace({ files: bundle.file, from, to });
            }

            const stat = await fs.stat(bundle.file);
            manifest.module = {
                url: path.basename(bundle.file),
                revision: await hasha.fromFile(bundle.file, { algorithm }),
                size: stat.size,
                type: "script"
            };

            /* write the final version of the manifest */
            await fs.writeJson(manifestPath, manifest);

            if (options.workbox) {
                for (const [lang, workbox] of Object.entries(options.workbox)) {
                    const additionalManifestEntries = workbox.additionalManifestEntries;
                    await manager.findModules(lang);
                    if (additionalManifestEntries) {
                        workbox.additionalManifestEntries = [
                            ...additionalManifestEntries, ...manager.extra[lang]
                        ];
                    }
                    else
                        workbox.additionalManifestEntries = manager.extra[lang];
                    injectManifest(workbox);
                }
            }
        }
    };
}

const processHtml = html => minify(html.toString(), { collapseWhitespace: true });
plugin.processHtml = processHtml;

const processCss = css => cssnano.process(css).then(result => result.css);
plugin.processCss = processCss;

plugin.external = imports => id => (
    id.startsWith(EXTERNAL_PREFIX) ||
    id.startsWith(RELATIVE_PREFIX) ||
    (imports && id in imports)
);

plugin.resolve = (manifestsPath, imports) => {
    const manifestsDirs = manifestsPath.split(":");
    const components = {};
    return id => {
        if (id.startsWith(RELATIVE_PREFIX))
            return id.slice(RELATIVE_PREFIX.length);
        if (imports && id in imports)
            return imports[id];
        if (!id.startsWith(EXTERNAL_PREFIX))
            throw new Error(`unknown module format "${id}"`);
        const externalModule = id.slice(EXTERNAL_PREFIX.length);
        const [componentName, resourceName] = externalModule.split(":");
        if (!(componentName in components))
            components[componentName] = loadComponent(manifestsDirs, componentName);
        const manifest = components[componentName].manifest;
        if (resourceName) {
            if (!(resourceName in manifest.resources))
                throw new Error(`unknown resource ${componentName}:${resourceName}`);
            return `${manifest.baseUrl}/${manifest.resources[resourceName].url}`;
        }
        return `${manifest.baseUrl}/${manifest.module.url}`;
    };
};

plugin.resources = (manifestsPath, indexJsonPath, resourcesPath) => {
    const manifestsDirs = manifestsPath.split(":");
    return {
        name: "resources",
        async writeBundle(bundle) {
            const indexJson = await fs.readJson(indexJsonPath);
            const selects = [];
            for (const [componentName, resourceNames] of Object.entries(indexJson.select)) {
                for (const resourceName of resourceNames)
                    selects.push(`${componentName}:${resourceName}`);
            }
            const selected = {};
            for (const manifestsDir of manifestsDirs) {
                for await (const manifestPath of klaw(manifestsDir)) {
                    if (!manifestPath.path.endsWith(".json"))
                        continue;
                    const manifest = await fs.readJson(manifestPath.path);
                    const componentName = manifest.name;
                    for (const [resourceName, resource] of Object.entries(manifest.resources)) {
                        for (const select of selects) {
                            if (!isMatch(`${componentName}:${resourceName}`, select))
                                continue;
                            const url = `${manifest.baseUrl}/${resource.url}`;
                            if (componentName in selected)
                                selected[componentName][resourceName] = url;
                            else
                                selected[componentName] = { [resourceName]: url };
                        }
                    }
                }
            }
            const content = "export default" + JSON.stringify(selected);
            await fs.writeFile(resourcesPath, content);
        }
    };
};

plugin.select = selected => {
    const selectSet = new Set(selected);
    for (const element of fs.readdirSync("elements")) { // TODO: pref
        const indexJson = fs.readJsonSync(path.join("elements", element, "index.json"));
        if (indexJson.select) {
            for (const [component, resources] of Object.entries(indexJson.select)) {
                for (const resource of resources)
                    selectSet.add(`${component}:${resource}`);
            }
        }
    }
    return Array.from(selectSet);
};


plugin.get = componentJsonPath => {
    const componentJson = fs.readJsonSync(componentJsonPath || COMPONENT_FILE);
    componentJson.name = formatComponentName(componentJson.name, componentJson.version);
    return componentJson;
};

plugin.computeRevision = (content, algorithm) => hasha(content, { algorithm });

function gitRoot() {
    let directory = process.cwd();
    do {
        if (fs.existsSync(path.join(directory, ".git")))
            return directory;
        directory = path.dirname(directory);
    }
    while (directory !== "/");
    throw new Error("could not find git directory");
}

plugin.gitRoot = gitRoot;

module.exports = { default: plugin };
